FROM ruby
MAINTAINER hello@simonbreiter.com
RUN apt-get -y update && apt-get -y install libicu-dev cmake && rm -rf /var/lib/apt/lists/*
RUN gem install github-linguist
RUN gem install gollum
RUN gem install github-markdown org-ruby
WORKDIR /wiki
CMD ["gollum", "--port", "80", "--mathjax"]
EXPOSE 80
