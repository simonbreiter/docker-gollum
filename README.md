# docker-gollum

Docker image of gollum wiki.

## Usage

Build and start container
```
docker-compose build; docker-compose up -d
```

Create git repository
```
cd wiki; git init
```
